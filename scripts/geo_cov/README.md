The files in this directory allow you to reproduce the results of Figure 1. 

First, we'll read the RIR files and check whether (i) the files are corrupted and (ii) whether the lookup process works properly. Please run:

~~~
python3 asn2rir.py
~~~

which should produce the following output:

~~~
############### Comparing data to summaries ###############
afrinic: received 2302 out of 2302 promised record, 100%
apnic: received 8804 out of 8804 promised record, 100%
arin: received 26873 out of 26873 promised record, 100%
lacnic: received 8496 out of 8496 promised record, 100%
ripencc: received 36376 out of 36376 promised record, 100%

################## Running random tests ###################
I ran 82851 random test lookups out of which 82851 (100%) succeeded.
~~~

each RIR delegation file carries a summary of the record count; hence, the first first part just compares the amount of data that it read against those summaries. If this does not yield 100% for all RIRs, please ensure that the file for the RIR that's not 100% is not corrupted (or just redownload it). For each asn record that we read, we produce one test case, i.e, we select one random entry from the range described and later use that for testing the lookup. 

Once all values show 100%, the asn lookup is good to go. Now you can run

~~~
python3 calc_coverage.py
~~~

which should generate a file at ../../results/coverage/rir.csv that includes the amount of inferred links and validatable links per RIR class. 
Finally, please run 

~~~
get_links_per_group.py
~~~

which generates a file at ../../results/links_per_class/rir_classes.csv that includes the links that are contained in each group. While this is not immediately helpful, it is used by later scripts. 
