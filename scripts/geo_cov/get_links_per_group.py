import sys
sys.path.insert(1, '../')
from utils import read_val_links,group_links_per_class
from asn2rir import TwoStageLookup

FN='../../results/links_per_class/rir_classes.csv'
TSL = TwoStageLookup()
valids = read_val_links()
with open(FN, 'wt') as out:
    data = group_links_per_class(valids, TSL)
    for comb in data.keys():
            out.write(comb+'='+','.join(data[comb])+'\n')
