import sys
sys.path.insert(1, '../')
from utils import read_val_links,group_links_per_class, read_asrank_links
from asn2rir import TwoStageLookup

FN='../../results/coverage/rir.csv'
TSL = TwoStageLookup()
valids = read_val_links()
infers = read_asrank_links()

vdata = group_links_per_class(valids, TSL)
idata = group_links_per_class(infers, TSL)

with open(FN, 'wt') as out:
    out.write('#Group|InferredLinks|ValidatableLinks\n')
    for comb in set(vdata.keys()).union(set(idata.keys())):
        out.write('|'.join([comb, str(len(idata[comb])), str(len(idata[comb].intersection(vdata[comb])))])+'\n')
