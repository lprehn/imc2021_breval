import sys
sys.path.insert(1, '../')
from utils import smartopen, find_le
import random, bisect
from collections import defaultdict
random.seed(12212102)

DATA_DIR='../../data/rir/'
FILES=['delegated-afrinic-extended-20180405','delegated-apnic-extended-20180405.gz','delegated-arin-extended-20180405','delegated-lacnic-extended-20180405','delegated-ripencc-extended-20180405.bz2'] 
IANA = 'iana_initial_assignments.csv'

def yield_lines():
    for file in FILES:
        with smartopen(DATA_DIR+file) as fileinput:
            for line in fileinput:
                yield line

class AsnDelegationLookup(object):

    def __init__(self):
        self.mapping = defaultdict(dict)
        self.starts = defaultdict(list)
        self.summaries = defaultdict(int)


    def add(self, start: str, value: int, rir: str, data: list):
        try:
            start = int(start)
        except ValueError:
            # older files (e.g., form 2010-12-31) have fatfingered entries like:
            # afrinic|MU|asn|5.1|1|20070504|allocated
            # let's just ignore them ...
            return
        stop = start+value-1
        self.mapping[rir][start] = (stop, data)
        bisect.insort(self.starts[rir], start)


    def _lookup_rir_entry(self, rir, asn):
        try:
           start = find_le(self.starts[rir], asn)
           stop, data = self.mapping[rir][start]
           if start <= asn <= stop:
               return (start, stop, data)
           raise ValueError
        except ValueError as ve:
            raise ve


    def lookup(self, query: str):
        query = int(query)
        min_diff, min_rir, min_data = float('inf'), 'NA', []
        self.potential_rirs = []

        for rir in self.mapping.keys():
            try:
                start, stop, data = self._lookup_rir_entry(rir, query)
                diff = stop-start
                self.potential_rirs.append((rir, diff))
                if diff < min_diff:
                    min_diff, min_rir, min_data = diff, rir, data
            except ValueError:
                continue

        return min_rir, min_data

    def add_summary(self, elems):
        registry, _, rtype, _, rcnt, _ = elems
        if rtype != 'asn':
            return
        self.summaries[registry] = int(rcnt)

    def get_potential_matches(self):
        return self.potential_rirs

    def show_comparison_to_summaries(self):
        print('############### Comparing data to summaries ###############')
        for rir in self.summaries:
            n = len(self.mapping[rir].keys())
            m = self.summaries[rir]
            print("%s: received %d out of %d promised record, %d%%" % (rir, n, m, int((n/m)*100)))
        print()

class ASNDelegationLookupTester:


    def __init__(self, lookup: AsnDelegationLookup):
        self.test_cases = []
        self.lookup = lookup
        for rir in lookup.mapping.keys():
            for start in lookup.mapping[rir].keys():
                stop = lookup.mapping[rir][start][0]
                query = random.randint(start, stop)
                self.test_cases.append((rir, query, start, stop))


    def run_tests(self):
        print('################## Running random tests ###################')
        tests, successes = 0, 0
        for rir, query, start, stop in self.test_cases:
            tests += 1
            lo_rir, _ = self.lookup.lookup(query)
            if rir == lo_rir:
                successes += 1
                continue
            print('Mismatch, ASN: %d (%d-%d), expected RIR: %s, retrieved RIR: %s' % (query, start, stop, rir, lo_rir))
            print('Potential:', self.lookup.get_potential_matches())
        print("I ran %d random test lookups out of which %d (%d%%) succeeded." % (tests, successes, int(successes*100/tests)))
        print()

def load_lookup():
    lookup = AsnDelegationLookup()
    for line in yield_lines():
        elems = line.split('|')

        # version line, comment, or empty line -> can be ignored safely
        if len(line) == 0 or elems[0][0].isdigit() or elems[0].startswith('#'):
            continue

        # this is a summary line
        if len(elems) == 6:
            lookup.add_summary(elems)
            continue

        registry, cc, rtype, start, value, date, status, *extra = elems
        if rtype != 'asn': continue
        data = [date, status]
        data.extend(extra)

        lookup.add(start, int(value), registry.rstrip(), data)
    return lookup

def load_iana_lookup():
    lookup = AsnDelegationLookup()
    with smartopen(DATA_DIR+IANA) as fileinput:
        for line in fileinput:
            therange, rir = line.split(',')
            if '-' in therange:
                start, stop = [int(x) for x in therange.split('-')]
            else: 
                start = stop = int(therange)
            rir = rir.lower().strip()

            if not rir in ['apnic', 'afrinic', 'arin', 'lacnic', 'ripencc']: continue
            lookup.add(start, stop-start+1, 'base-'+rir, [])
    return lookup

class TwoStageLookup:
    def __init__(self):
        self.AsnLookup = load_lookup()
        self.IanaAsnLookup = load_iana_lookup()

    def lookup(self, asn):
        min_rir, _ = self.AsnLookup.lookup(asn)
        if min_rir == 'NA':
            return self.IanaAsnLookup.lookup(asn)[0]
        else:
            return min_rir

    def get(self, asn, default = 'NA'):
        rir = self.lookup(asn)
        return (rir, default)[rir == 'NA']

if __name__ == "__main__":

    # loading the tester
    AsnLookup = load_lookup()

    # checking in-file consistency
    AsnLookup.show_comparison_to_summaries()

    # doing a bunch of random checks
    tester = ASNDelegationLookupTester(AsnLookup)
    tester.run_tests()

