The files in this directory allow you to reproduce the results of Figure 2.

First we need to generate the mapping from asns to topological classes. Please ensure that the data directory contains the customer cone related files, and then run:

~~~
produce_topo_classifier.py
~~~

this will produce a file at ../../results/topo_classifier/classes.csv that associates each ASN with a class. Once this file is written, you can run

~~~
python3 calc_coverage.py
~~~

to produce ../../results/topo_classifier/classes.csv which again shows for each class the number of inferred and validatable links (in this order). Finally, please run:

~~~
get_links_per_group.py
~~~

to generate the ../../results/topo_classifier/classes.csv file which contains the links of each class (to be used later). 
