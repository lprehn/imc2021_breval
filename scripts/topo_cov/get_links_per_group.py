import sys
sys.path.insert(1, '../')
from utils import read_val_links,group_links_per_class

def read_as_classes():
   file='../../results/topo_classifier/classes.csv'
   with open(file, 'r') as INPUT:
       classes = dict()
       for line in INPUT:
           if line.startswith('#'): continue
           asn, asclass = line.split('|')
           asn = int(asn)
           classes[asn] = asclass.rstrip()
   return classes

FN='../../results/links_per_class/topo_classes.csv'
valids = read_val_links()
classes = read_as_classes()
with open(FN, 'wt') as out:
    data = group_links_per_class(valids, classes)
    for comb in data.keys():
            out.write(comb+'='+','.join(data[comb])+'\n')
