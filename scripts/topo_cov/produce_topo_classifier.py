import sys
sys.path.insert(1, '../')
from utils import read_ppdc
from collections import defaultdict

def get_hypergiants():
    Hypergiants = set()
    Hypergiants.add(714)   # Verizon
    Hypergiants.add(16509) # Amazon
    Hypergiants.add(32934) # Facebook
    Hypergiants.add(15169) # Google
    Hypergiants.add(20940) # Akamai
    Hypergiants.add(10310) # Yahoo
    Hypergiants.add(2906)  # Netflix
    Hypergiants.add(6939)  # Hurrican Electric
    Hypergiants.add(16276) # OVH
    Hypergiants.add(22822) # Limelight
    Hypergiants.add(8075)  # Microsoft
    Hypergiants.add(13414) # Twitter
    Hypergiants.add(46489) # Twitch
    Hypergiants.add(13335) # Cloudflare
    Hypergiants.add(15133) # Verizon
    return Hypergiants

def get_tierones():
    Tierones = set()
    Tierones.add(174)   # Cogent (Clique)
    Tierones.add(209)   # Lumen/CL/Level3 (Clique)
    Tierones.add(701)   # Verizon (Wiki+Clique)
    Tierones.add(1239)  # Sprint (Wiki+Clique)
    Tierones.add(1299)  # Telia (Wiki)
    Tierones.add(2828)  # XO Comm (Clique)
    Tierones.add(2914)  # NTT (Wiki+Clique)
    Tierones.add(3257)  # GTT (Wiki+Clique)
    Tierones.add(3320)  # DT (Wiki+Clique)
    Tierones.add(3549)  # Lumen/CL/Level3 (Clique)
    Tierones.add(3356)  # Lumen/CL/Level3 (Wiki+Clique)
    Tierones.add(3491)  # PCCW (Wiki)
    Tierones.add(3549)  # Lumen/CL/Level3 (Wiki)
    Tierones.add(5511)  # Orange (Wiki+Clique)
    Tierones.add(6453)  # Tata (Wiki+Clique)
    Tierones.add(6461)  # Zayo (Wiki)
    Tierones.add(6762)  # Telecom Italia (Wiki)
    Tierones.add(6830)  # LG (Wiki)
    Tierones.add(7018)  # ATT (Wiki+Clique)
    Tierones.add(12956) # Telxius (Wiki)
    return Tierones

# load customer cones
ppdc = read_ppdc()

classes = dict()
for asn in ppdc:
    # provides transit for someone
    if len(ppdc[asn]) > 1:
        classes[asn] = 'TRANSIT'
        continue

    # no trabnsit, multihomed?
    classes[asn] = 'STUB'


for asn in get_hypergiants():
    classes[asn] = 'HYPERGIANT'

for asn in get_tierones():
    classes[asn] = 'TIERONE'

with open('../../results/topo_classifier/classes.csv', 'wt') as out:
    for asn in sorted(list(classes.keys())):
        out.write(str(asn)+'|'+classes[asn]+'\n')
