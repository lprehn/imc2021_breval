import sys
sys.path.insert(1, '../')
from utils import read_val_links,group_links_per_class, read_asrank_links

def read_as_classes():
   file='../../results/topo_classifier/classes.csv'
   with open(file, 'r') as INPUT:
       classes = dict()
       for line in INPUT:
           if line.startswith('#'): continue
           asn, asclass = line.split('|')
           asn = int(asn)
           classes[asn] = asclass.rstrip()
   return classes


FN='../../results/coverage/topo.csv'
classes = read_as_classes()
valids = read_val_links()
infers = read_asrank_links()

vdata = group_links_per_class(valids, classes)
idata = group_links_per_class(infers, classes)

with open(FN, 'wt') as out:
    for comb in set(vdata.keys()).union(set(idata.keys())):
        out.write('|'.join([comb, str(len(idata[comb])), str(len(idata[comb].intersection(vdata[comb])))])+'\n')
