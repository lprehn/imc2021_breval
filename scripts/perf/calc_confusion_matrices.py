import sys
sys.path.insert(1, '../')
from utils import *
import bz2
from collections import defaultdict

def read_link_classes():
    DIR='../../results/links_per_class/'
    links_per_class = defaultdict(set)
    for file, ctype in [('topo_classes.csv', 'as'), ('rir_classes.csv', 'rir')]:
        with open(DIR+file, 'r') as INPUT:
            for line in INPUT:
                theclass, linksraw = line.split('=', 1)
                links_per_class[ctype+'|'+theclass] = set(linksraw.split(','))
                links_per_class['total|all-all'].update(set(linksraw.split(',')))
    return links_per_class

def cofusion_matrix_per_class(classes, validation, inference, name, start = False):
    '''
    True == P2P
    False == P2C
    '''
    mode = ('a', 'w')[start]
    with open('../../results/perf/confusion_matrices.csv', mode) as out:
        for classkey in classes:
            confusion = defaultdict(int)
            p2p_links = 0
            p2c_links = 0
            for link in classes[classkey]:
                valid = validation.get(link, 'NA')
                infer = inference.get(link, 'NA')
                if valid == 0: p2p_links += 1
                #if valid == 0 and 'STUB' in classkey and 'TIERONE' in classkey: print('#', link)
                if valid == 1 or valid == -1: p2c_links += 1
                if valid == 'NA' or infer == 'NA': continue
                if valid == 0 and infer == 0: confusion['TP'] += 1
                if valid == 0 and infer != 0: confusion['FN'] += 1
                if valid != 0 and infer == 0: confusion['FP'] += 1
                if valid != 0 and infer != 0: confusion['TN'] += 1
                if valid != 0 and infer != 0 and valid != infer: 
                    confusion['DM'] += 1 # direction missmatch (not actually part of confusion metrics :P)
            out.write('|'.join([str(x) for x in [name, classkey, confusion['TP'], confusion['FP'], confusion['TN'], confusion['FN'], confusion['DM'], p2p_links, p2c_links]])+'\n')

# load links
links_per_class = read_link_classes()

# load inferences and validation
validation = read_val_lookup()
infer_asrank = read_asrank_lookup()
infer_toposcope = read_topo_lookup()
infer_problink = read_prob_lookup()

# produce the confusion matrices
cofusion_matrix_per_class(links_per_class, validation, infer_asrank, 'asrank', start = True)
cofusion_matrix_per_class(links_per_class, validation, infer_toposcope, 'toposcope')
cofusion_matrix_per_class(links_per_class, validation, infer_problink, 'problink')
