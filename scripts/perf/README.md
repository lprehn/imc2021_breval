This directory contains the scripts needed to reproduce Table 1-3.
We assume that before running any script in this directory, you already executed the scripts in the geo_cov and topo_cov directories---these generate the files that are used as inputs.

First, please calculate the confusion matrices by running:

~~~
python3 calc_confusion_matrices.py
~~~

This will generate a single file at ../../results/perf/confusion_matrices.csv which stores a single confusion matrix plus extra information per line as:

~~~
algorithm|class|TruePositives|FalsePositives|TrueNegatives|FalseNegatives|DirectionMismatches|NumP2PLinks|NumP2CLinks
~~~

Once this file is generate, you can generate the validation tables within the paper by running:

~~~
python3 calc_performance.py
~~~

This will print the Latex code of each table to stdout. Please note: Just copy-pasting the output will not lead to nice formatting right-away. Please add the following lines before the \begin{document} in your latex file:

~~~
\usepackage{xcolor}
\usepackage{colortbl}
\definecolor{goodgreen}{HTML}{c6ebc9}
\definecolor{badyellow}{HTML}{ffefa1}
\definecolor{badorange}{HTML}{fdc099}
\definecolor{badred}{HTML}{fa9191}
~~~
