import math, sys
import io
from collections import defaultdict

def transform_single_name(name):
    if name == 'STUB': return 'S'
    if name == 'TRANSIT': return 'TR'
    if name == 'TIERONE': return 'T1'
    if name == 'HYPERGIANT': return 'G'
    if name == 'arin': return 'AR'
    if name == 'ripencc': return 'R'
    if name == 'apnic': return 'AP'
    if name == 'afrinic': return 'AF'
    if name == 'lacnic': return 'L'
    if name == 'all': return 'Total'
    return "NA"

def transform_groupname(name):
    a,b = name.strip().split('-')
    na, nb = transform_single_name(a), transform_single_name(b)
    if a == b:
        if a == 'Total': return 'Total'
        return na+'°'
    else:
        return na+'-'+nb

def print_to_string(*args, **kwargs):
    output = io.StringIO()
    print(*args, file=output, **kwargs)
    contents = output.getvalue()
    output.close()
    return contents

def calc(TP, FP, TN, FN):

    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    precision_inv = TN/(TN+FN)
    recall_inv = TN/(TN+FP)
    MCC = ((TP*TN) - (FP*FN))/math.sqrt((TP+FP)*(TP+FN)*(TN+FP)*(TN+FN))
    return [precision, recall, precision_inv, recall_inv, MCC]

def format_cell(value, total_val):
    if value > total_val*1.01 or value >= 0.999: return "\\cellcolor{goodgreen} %.3f" % value
    if total_val*0.99 < value <= total_val*1.01:  return "\\cellcolor{white} %.3f" % value
    if total_val*0.95 < value <= total_val*0.99:  return "\\cellcolor{badyellow} %.3f" % value
    if total_val*0.9 < value <= total_val*0.95:  return "\\cellcolor{badorange} %.3f" % value
    return "\\cellcolor{badred} %.3f" % value

FILE='../../results/perf/confusion_matrices.csv'
totals = dict()
# first only read totals:
with open(FILE, 'r') as INPUT:
    results_per_algo = defaultdict(lambda: defaultdict(list))
    for line in INPUT:
        if line.startswith('#'): continue
        if not any(line.startswith(x) for x in ['toposcope|total|', 'asrank|total|', 'problink|total|']): continue
        algo, ctype, theclass, tp, fp, tn, fn, _, pN, cN = line.split('|')
        vals  = calc(float(tp), float(fp), float(tn), float(fn))
        vals.extend([int(pN), int(cN)])
        totals[algo] = vals

with open(FILE, 'r') as INPUT:
    results_per_algo = defaultdict(lambda: defaultdict(list))
    for line in INPUT:
        if line.startswith('#'): continue
        if '|0|0|0|0' in line: continue
        algo, ctype, theclass, tp, fp, tn, fn, _, pn, cn = line.split('|')
        lim = 500
        if int(pn) + int(cn) < lim: continue
        vals  = calc(float(tp), float(fp), float(tn), float(fn))
        vals = [format_cell(x, totals[algo][i]) for i, x in enumerate(vals)]
        vals.insert(2, int(pn))
        vals.insert(5, int(cn))
        results_per_algo[algo][ctype].append("%s & %s & %s & %s & %s & %s & %s & %s \\\\" % (transform_groupname(theclass), *vals))

    print("\\setlength\\tabcolsep{3.0pt}")
    for algo in results_per_algo:
        print("\\begin{table}[]")
        print("\t\centering")
        print("\t\\begin{tabular}{l|c|c|c|c|c|c|c}")
        print('\t\tClass', '$PPV_{P}$','$TPR_{P}$', '$LC_{P}$', '$PPV_{C}$','$TPR_{C}$', '$LC_{C}$', 'MMC', sep = ' & ', end = '\\\\\n')
        print('\t\t\\hline')
        for ctype in sorted(results_per_algo[algo])[::-1]:
            for result in sorted(results_per_algo[algo][ctype]):
                print("\t\t"+result)
            print('\t\t\\hline')
        if algo == 'asrank': head = 'ASRank'
        elif algo == 'toposcope': head = 'Toposcope'
        else: head ='ProbLink'
        print("\t\end{tabular}")
        print("\t\caption{Per group validation table for "+head+"}")
        print("\t\label{tab:perf:"+head+"}")
        print("\end{table}")
