import bisect
import gzip, bz2
import subprocess
from collections import defaultdict
GITHEAD=subprocess.check_output(["git","rev-parse","--show-toplevel"]).rstrip().decode("utf-8")

INF_DIR=GITHEAD+'/data/inferences/'
VAL_DIR=GITHEAD+'/data/validation/'
PPDC_DIR=GITHEAD+'/data/ppdc/'

def is_reserved(asn):
    asn = int(asn)
    if asn == 0: return True
    if asn == 112: return True
    if asn == 23456: return True
    if 64496 <= asn <= 65551: return True
    if 4200000000 <= asn <= 4294967295: return True
    return False

def read_lookup(fileobj, delim = '|'):
    '''
    We ignore sibling 2 sibling links and overload the
    meaning of the value 1.
    '''
    data = dict()
    for line in fileobj:
        if line.startswith('#'): continue
        a, b, rel = line.split(delim)
        if is_reserved(a) or is_reserved(b): continue # ignore reserved ASes
        if ',' in rel: continue                       # ignore ambigious labels
        rel = int(rel)
        if rel == 1: continue                         # sinbling 2 sibling
        data[a+'|'+b] = rel
        data[b+'|'+a] = rel*-1
    return data

def read_asrank_lookup():
    file=INF_DIR+'asrank_20180401.bz2'
    with smartopen(file) as fo:
        return read_lookup(fo)

def read_prob_lookup():
    file=INF_DIR+'problink_20180401'
    with smartopen(file) as fo:
        return read_lookup(fo)

def read_topo_lookup():
    file=INF_DIR+'toposcope_20180401'
    with smartopen(file) as fo:
        return read_lookup(fo)

def read_val_lookup():
    file=VAL_DIR+'communities_relationships_201804.txt'
    with smartopen(file) as fo:
        return read_lookup(fo, delim = None)




def read_links(fileobj, delim = '|'):
    links = set()
    for line in fileobj:
        if line.startswith('#'): continue
        try:
            a, b, rel = line.split(delim)
        except:
            print(line)
        if is_reserved(a) or is_reserved(b): continue # ignore reserved ASes
        if ',' in rel: continue                       # ignore ambigious labels

        if a < b: links.add(a+'|'+b)                  # make links undirected
        else: links.add(b+'|'+a)
    return links

def read_asrank_links():
    file=INF_DIR+'asrank_20180401.bz2'
    with smartopen(file) as fo:
        return read_links(fo)

def read_prob_links():
    file=INF_DIR+'problink_20180401'
    with smartopen(file) as fo:
        return read_links(fo)

def read_topo_links():
    file=INF_DIR+'toposcope_20180401'
    with smartopen(file) as fo:
        return read_links(fo)

def read_val_links():
    file=VAL_DIR+'communities_relationships_201804.txt'
    with smartopen(file) as fo:
        return read_links(fo, delim = None)


def smartopen(filename):
    if filename.endswith('.gz'):
        return gzip.open(filename, 'rt')
    elif filename.endswith('.bz2'):
        return bz2.open(filename, 'rt')
    else:
        return open(filename, 'r')

def find_le(a, x):
    'Find rightmost value less than or equal to x'
    i = bisect.bisect_right(a, x)
    if i:
        return a[i-1]
    raise ValueError

def group_links_per_class(links, classes, default=False):
    lpc = defaultdict(set)
    NA_s = set()
    for link in links:
        a_raw,b_raw = link.split('|')
        a = classes.get(int(a_raw))
        b = classes.get(int(b_raw))
        if a == 'NA': NA_s.add(a_raw)
        if b == 'NA': NA_s.add(b_raw)
        if default:
            if a is None: a = 'NA'
            if b is None: b = 'NA'
        else:
            if a == -1 or b == -1 or a is None or b is None: continue
        if a < b: lpc[a+'-'+b].add(link)
        else: lpc[b+'-'+a].add(link)
    return lpc

def read_ppdc():
    data = dict()
    with bz2.open(PPDC_DIR+'20180401.ppdc-ases.txt.bz2', 'rt') as INPUT:
        for line in INPUT:
            if line.startswith('#'): continue
            a, *cone = [int(x) for x in line.split()]
            if is_reserved(a): continue
            data[a] = cone
    return data
