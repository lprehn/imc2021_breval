The files in this directory allow you to reproduce the results of Figure 3 as well as all its alternate versions. 
The script assumes that you have a provider-peer-observed customer cone file (e.g., from CAIDA's repo) as well as a file that contains node and transit degrees. 
We *do not* provide tools to generate this second file as it's a by-product of a different internal data pipeline.
To still allow you to execute this script and reproduction of our results, we provide the snapshot that we used in our analysis.

To run this analysis, please execute:

~~~
python3 calc_transit_link_sizes.py
~~~

This will produce three files in ../../results/transit_imbalance/ each of which shows the desired degree for the incident ASes of each link and whether the link
is contained in the validation data.

 


