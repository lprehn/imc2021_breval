import sys
sys.path.insert(1, '../')
from utils import read_ppdc, read_asrank_links, read_val_links
import bz2, gzip
from collections import defaultdict

OUT_DIR='../../results/transit_imbalance/'

def load_types():
    FILE='../../results/topo_classifier/classes.csv'
    classes = dict()
    with open(FILE, 'r') as INPUT:
        for line in INPUT:
            if line.startswith('#'): continue
            asn, asclass = line.split('|')
            classes[int(asn)] = asclass.rstrip()
    return classes

def load_sizes():
    nsizes, tsizes = defaultdict(int), defaultdict(int)
    FILE='../../results/as_sizes/degrees.csv'
    with open(FILE, 'rt') as fileinput:
        for line in fileinput:
            if line.startswith('#'): continue
            asn, nd, td = line.split('|')
            asn = int(asn)
            nsizes[asn] = int(nd)
            tsizes[asn] = int(td)
    return nsizes, tsizes

def annotate(name, lookup, classes):
    with gzip.open(OUT_DIR+name, 'wt') as out:
        out.write('#AsnA|AsnB|SizeA|SizeB|LinkInValidationData\n')
        for link in links_infer:
            a, b = link.split('|')
            a, b = int(a), int(b)
            if classes.get(a, 'NA') != 'TRANSIT' or classes.get(b, 'NA') != 'TRANSIT': continue    # we only care about transit interconnections
            asize = lookup[a]
            bsize= lookup[b]

            if asize < bsize:
                out.write('|'.join([str(a), str(b), str(asize), str(bsize), str(link in links_valid)])+'\n')
            else:
                out.write('|'.join([str(b), str(a), str(bsize), str(asize), str(link in links_valid)])+'\n')

def load_ccsizes():
    ppdcs = read_ppdc()
    ccsizes = defaultdict(int)
    for asn in ppdcs:
        ccsizes[asn] = len(ppdcs[asn])
    return ccsizes

ccsizes = load_ccsizes()
nsizes, tsizes = load_sizes()
classes = load_types()
links_valid = read_val_links()
links_infer = read_asrank_links()


annotate('ppdcs.csv.gz', ccsizes, classes)
annotate('ndegrees.csv.gz', nsizes, classes)
annotate('tdegrees.csv.gz', tsizes, classes)
