#!/bin/bash

ROOT=$(git rev-parse --show-toplevel)
DATA_DIR=${ROOT}/data/

function populate_and_check {
    wget -q -t 3 -O $2 $1
    if [ -f "$2" ]; then
        echo "Successfully populated: $2"
    else
        echo "Failed to populate: $2"
    fi
}


echo "======================== Starting to populate data/rir/ ========================"
populate_and_check https://ftp.apnic.net/stats/apnic/2018/delegated-apnic-extended-20180405.gz ${DATA_DIR}rir/delegated-apnic-extended-20180405.gz
populate_and_check https://ftp.apnic.net/stats/afrinic/2018/delegated-afrinic-extended-20180405 ${DATA_DIR}rir/delegated-afrinic-extended-20180405
populate_and_check https://ftp.arin.net/pub/stats/arin/delegated-arin-extended-20180405 ${DATA_DIR}rir/delegated-arin-extended-20180405
populate_and_check https://ftp.lacnic.net/pub/stats/lacnic/delegated-lacnic-extended-20180405 ${DATA_DIR}rir/delegated-lacnic-extended-20180405
populate_and_check https://ftp.apnic.net/stats/ripe-ncc/2018/delegated-ripencc-extended-20180405.bz2 ${DATA_DIR}rir/delegated-ripencc-extended-20180405.bz2
echo ""

echo "======================== Starting to partially populate data/inferences/ ========================"
populate_and_check https://publicdata.caida.org/datasets/as-relationships/serial-1/20180401.as-rel.txt.bz2 ${DATA_DIR}inferences/asrank_20180401.bz2
populate_and_check https://github.com/Zitong-Jin/TopoScope/blob/master/asrel/asrel_201804.txt ${DATA_DIR}inferences/toposcope_20180401
echo ""

echo "======================== Starting to populate data/ppdc/ ========================"
https://publicdata.caida.org/datasets/as-relationships/serial-1/20180401.ppdc-ases.txt.bz2 ${DATA_DIR}ppdc/20180401.ppdc-ases.txt.bz2
echo ""


