If you run

~~~
./populate.sh
~~~

the script will start to download *most* of the necessary files. Please note that, in addition, you have to request the following files from their rightful owners/authors:

### data/inferences/problink_20180401
You can get this file by contacting the authers of the problink paper, in particular, Yuchen Jin \<yuchenj@cs.washington.edu\>. It includes a snapshot of problink's inferences for 2018-04-01. 


### data/validation/communities_relationships_201804.txt
Yuchen Jin \<yuchenj@cs.washington.edu\> (POC for ProbLink) and Zitong Jin \<jinzt19@mails.tsinghua.edu.cn\> (POC for TopoScope) provided this file to us. Both of them told us that they initially received by requesting CAIDA. The file contains the community-based validation data for 2018-04-01. 
 
As the last file is required to arrive at most of our results, please obtain those files before starting any follow-up scripts. 

