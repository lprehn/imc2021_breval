This repository contains all scripts necessary to reproduce the major results presented in:

> Prehn, Lars and Feldmann, Anja. "How biased is our Validation (Data) for AS Relationships?" Proceedings of the ACM Internet Measurement Conference. 2021.

You can also find a copy of the publication in this git, see [here](/imc21_prehn_breval_crv2109292.pdf).<br>
All scripts in this repository can be found in /scripts. In particular, the structure within scripts/ looks as follows:
~~~
.
├── geo_cov                                 # Reproduce Results for Figure 1
│   ├── asn2rir.py
│   ├── calc_coverage.py
│   ├── get_links_per_group.py
│   └── README.md
├── perf                                    # Reproduce Results for Table 1-3
│   ├── analyze_correlations_bu.py
│   ├── calc_confusion_matrices.py
│   ├── calc_performance.py
│   └── README.md
├── populate_datasets                       # Download and populate all the data sources needed for the scripts
│   ├── populate.sh
│   └── README.md
├── topo_cov                                # Reproduce Results for Figure 2
│   ├── calc_coverage.py
│   ├── get_links_per_group.py
│   ├── produce_topo_classifier.py
│   └── README.md
├── transit_link_imbalance                  # Reproduce Results for Figure 3
│   ├── calc_transit_link_sizes.py
│   └── README.md
└── utils.py                                # Functions to read/write data sources easily
~~~

Each sub-directory contains a README.md files that explains the requirements, script calls, and outputs that can be generated. The first directory that needs to entered is scripts/populate_datasets/. 

Please notice that this repository is licensed under the CRAPL license; please treat it as such. 
In case you have any questions, please contact

Lars Prehn \<lprehn@mpi-inf.mpg.de\><br>
Anja Feldmann \<anja@mpi-inf.mpg.de\>
